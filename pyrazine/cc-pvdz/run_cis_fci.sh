#!/bin/bash -x
#SBATCH -N 1  # 1 noeud
#SBATCH -n 1  # 1 processus
#SBATCH -c 36  # 36 coeurs

INPUT=$1
source ~/tmpdir/qp2/quantum_package.rc

set -e 
qp set_file $INPUT
qp set determinants n_states 1 
qp set determinants read_wf True
qp set determinants s2_eig False
qp set determinants n_det_max 1e7
qp run scf > ${INPUT}.scf.out
qp set_frozen_core > ${INPUT}.fc.out
qp set determinants n_states 10
qp run cis > ${INPUT}.cis.out
qp run save_natorb > ${INPUT}.natorb.out
qp run scf > ${INPUT}.scf.out
qp set_frozen_core > ${INPUT}.fc.out
qp set determinants n_states 1
qp run fci > ${INPUT}.fci.out || :
qp run save_natorb > ${INPUT}.natorb.out
qp set determinants n_det_max 1e8
qp run fci > ${INPUT}.nofci.out


