#!/bin/bash -x
#SBATCH -N 1  # 1 noeud
#SBATCH -n 1  # 1 processus
#SBATCH -c 36  # 36 coeurs

INPUT=$1
source ~/tmpdir/qp2/quantum_package.rc 

set -e 

./localize.sh ${INPUT}.ang ${QP_ROOT}/data/basis/cc-pvdz ${INPUT}
qp set_file ${INPUT}.localized
qp set determinants n_states 1 
qp set determinants read_wf False
qp set determinants s2_eig False
qp set determinants n_det_max 1e8
qp run fci > ${INPUT}.localized.nofci.out
