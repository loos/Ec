#!/bin/bash

# localize.sh xyz basis ezfio charge multiplicity

XYZ=$1
BASIS=$2
EZFIO=${3%/}
CHARGE=${4:-0}
MULT=${5:-1}

./localize \
	-b $BASIS \
	-x $XYZ \
	-i $EZFIO \
	-c $CHARGE \
	-m $MULT \
	-o ${EZFIO}.mo \
	-s "sp" 

cp -r ${EZFIO} ${EZFIO}.localized

source ~/tmpdir/qp2/quantum_package.rc 

qp set_file ${EZFIO}.localized
cat ${EZFIO}.mo | qp set mo_basis mo_coef


