#!/bin/bash -x
#SBATCH -N 10  # 1 noeud
#SBATCH -n 10  # 1 processus
#SBATCH -c 36  # 36 coeurs

INPUT=$1
source ~/tmpdir/qp2/quantum_package.rc 

set -e 

cp -r ${INPUT}.localized ${INPUT}.restart
qp set_file ${INPUT}.restart
qp set determinants n_states 1 
qp set determinants read_wf True
qp set determinants s2_eig False
qp set determinants n_det_max 1e8
qp srun fci > ${INPUT}.restart2.nofci.out
